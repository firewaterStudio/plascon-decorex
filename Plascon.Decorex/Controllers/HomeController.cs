﻿using System.Web.Mvc;

namespace Plascon.Decorex.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int t = 0)
        {
            ViewBag.FromThankYou = t == 1;

            return View();
        }

        public ActionResult Index2(int t = 0)
        {
            ViewBag.FromThankYou = t == 1;

            return View();
        }

        public ActionResult ThankYou()
        {
            return RedirectToAction("Index", new { t = 1 });
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

    }
}